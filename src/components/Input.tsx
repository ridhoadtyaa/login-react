import { twclsx } from '../lib/twclsx';

import { createElement, forwardRef } from 'react';

type InputProps = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

const Input = forwardRef<HTMLInputElement, InputProps>(({ type = 'text', className: c, ...props }, ref) => {
	const className = twclsx('rounded-lg', 'px-2', 'focus:outline-0', 'placeholder:text-sm', c);
	return createElement('input', { ...props, ref, autoComplete: 'off', className, type });
});

Input.displayName = 'Input';

export default Input;
