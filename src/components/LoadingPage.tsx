import { ImSpinner2 as Spinner } from 'react-icons/im';

const LoadingPage = () => {
	return (
		<div className="fixed top-0 left-0 right-0 bottom-0 z-50 flex h-screen w-full items-center justify-center">
			<Spinner className="h-24 w-24 animate-spin text-slate-600" />
		</div>
	);
};

export default LoadingPage;
