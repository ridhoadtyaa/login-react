import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import DashboardPage from './pages/Dashboard';
import LoginPage from './pages/Login';

const router = createBrowserRouter([
	{
		path: '/',
		element: <DashboardPage />,
	},
	{
		path: '/login',
		element: <LoginPage />,
	},
]);

const App = () => <RouterProvider router={router} />;

export default App;
