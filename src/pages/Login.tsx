import { useDispatch, useSelector } from 'react-redux';
import Button from '../components/Button';
import Input from '../components/Input';
import { twclsx } from '../lib/twclsx';
import { useState, useEffect } from 'react';
import { login } from '../feature/authSlice';
import { useNavigate } from 'react-router-dom';
import { RootState } from '../app/store';

const LoginPage: React.FunctionComponent = () => {
	const user = useSelector((state: RootState) => state.auth.user);
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const loginHandler = (e: React.SyntheticEvent) => {
		e.preventDefault();

		if (email === 'ridho@gmail.com' && password === 'test') {
			dispatch(login({ email, password }));
			navigate('/');
		} else {
			alert('Email or password is wrong');
			setPassword('');
		}
	};

	useEffect(() => {
		if (user) {
			navigate('/');
		}
	}, [user]);

	return (
		<div className="max-w-xl w-11/12 mx-auto flex min-h-screen">
			<div className="rounded-lg bg-white shadow-md border m-auto w-full md:w-10/12">
				<div className="p-4 border-b">
					<h3 className="text-lg font-semibold">Login Page</h3>
				</div>
				<div className="p-4">
					<form onSubmit={loginHandler}>
						<div className="mb-4">
							<label htmlFor="email" className="text-sm font-medium">
								Email
							</label>
							<Input
								placeholder="Enter your email"
								id="email"
								type="email"
								autoFocus
								className={twclsx('border w-full', 'p-2 mt-1')}
								onChange={e => setEmail(e.target.value)}
								value={email}
								required
							/>
						</div>
						<div className="mb-4">
							<label htmlFor="password" className="text-sm font-medium">
								Password
							</label>
							<Input
								placeholder="Enter your password"
								id="password"
								type="password"
								className={twclsx('border w-full', 'p-2 mt-1')}
								value={password}
								onChange={e => setPassword(e.target.value)}
								required
							/>
						</div>
						<Button className={twclsx('px-4 py-2', 'rounded-md bg-black', 'text-white text-sm font-semibold')}>Login</Button>
					</form>
				</div>
			</div>
		</div>
	);
};

export default LoginPage;
