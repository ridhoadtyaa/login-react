import { useDispatch, useSelector } from 'react-redux';
import Button from '../components/Button';
import type { RootState } from '../app/store';
import { twclsx } from '../lib/twclsx';
import LoadingPage from '../components/LoadingPage';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { logout } from '../feature/authSlice';

const DashboardPage: React.FunctionComponent = () => {
	const user = useSelector((state: RootState) => state.auth.user);
	const dispatch = useDispatch();
	const navigate = useNavigate();

	useEffect(() => {
		if (!user) {
			navigate('/login');
		}
	}, [user]);

	if (!user) {
		return <LoadingPage />;
	}
	return (
		<div className="max-w-xl mx-auto w-11/12">
			<div className="flex items-center justify-between mt-4">
				<h1 className="text-2xl">Dashboard</h1>
				<Button className={twclsx('px-4 py-2', 'rounded-md bg-black', 'text-white text-sm font-semibold')} onClick={() => dispatch(logout())}>
					Logout
				</Button>
			</div>
			<h2 className="text-xl mt-10">Welcome, {user.email}</h2>
		</div>
	);
};

export default DashboardPage;
